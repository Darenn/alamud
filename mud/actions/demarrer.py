from .action import Action2
from mud.events import DemarrerEvent

class DemarrerAction(Action2):
    EVENT = DemarrerEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "demarrer"
