from .event import Event2

class DemarrerEvent(Event2):
    NAME = "demarrer"

    def perform(self):
        if not self.object.has_prop("demarrable"):
            self.fail()
            return self.inform("demarrer.failed")
        self.inform("demarrer")
